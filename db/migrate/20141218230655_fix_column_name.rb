class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :images, :path, :image_path
  end
end
